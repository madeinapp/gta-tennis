<!DOCTYPE HTML>
	<head>
    	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-58643-34"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-58643-34');
</script>
    

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Pastebin.com - Page Removed</title>
		<link rel="shortcut icon" href="/favicon.ico" />
		<script src="/js/jquery.min.js"></script>
		<script src="/js/pastebin.min.v3.js"></script>
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<script>
  window.googletag = window.googletag || {cmd: []};
  googletag.cmd.push(function() {
    googletag.defineSlot('/7346874/Hellobar-adunits/176', [1, 1], 'div-gpt-ad-1565040785112-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
</script>

            
		<link href="/i/pastebin.min.v9.css?1575389335" rel="stylesheet" type="text/css" />
		<!--[if lt IE 10]>
			<link href="/i/pastebin.ie8.css" rel="stylesheet" type="text/css" />
		<![endif]-->

 
		<style>body{-webkit-text-size-adjust:none;}</style>
				<meta property="fb:app_id" content="231493360234820" />
		<meta property="og:title" content="Pastebin.com - Page Removed" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="https://pastebin.com/ue5Fg3vV" />
		<meta property="og:image" content="https://pastebin.com/i/facebook.png" />
		<meta property="og:site_name" content="Pastebin" />
		<meta name="google-site-verification" content="jkUAIOE8owUXu8UXIhRLB9oHJsWBfOgJbZzncqHoF4A" />
		<link rel="canonical" href="https://pastebin.com/ue5Fg3vV" />
				<meta name="viewport" content="width=device-width, initial-scale=0.75, maximum-scale=1.0, user-scalable=yes">
		<script type="text/javascript">
			if (top != self)
				top.location.href = location.href;
		</script>
	</head>
	<body>
	<div id="main_frame">
		<div id="jq-dropdown-1" class="jq-dropdown jq-dropdown-anchor-right jq-dropdown-scroll">
			<ul class="jq-dropdown-menu">
				
				<li class="lih_640">
					<form class="search_form_li" name="search_form_li" method="get" action="/search" id="cse-search-box-li">
						<input class="search_input_li" type="text" name="q" size="5" value="" placeholder="search..." />
					</form>

				</li>
				<li class="lih_div"></li>
				<li onclick="location.href='<?php echo env('APP_URL')?>/signup'" class="dd_su">Sign Up</li>
				<li onclick="location.href='<?php echo env('APP_URL')?>/login'" class="dd_lo">Login</li>
				<li class="lih_div"></li>
				<li onclick="location.href='<?php echo env('APP_URL')?>/api'" class="lih_640">API</li>
				<li onclick="location.href='<?php echo env('APP_URL')?>/faq'" class="lih_640">FAQ</li>
				<li onclick="location.href='<?php echo env('APP_URL')?>/tools'" class="lih_640">Tools</li>
				<li onclick="location.href='<?php echo env('APP_URL')?>/archive'" class="lih_640">Archive</li>			</ul>
		</div>
		<div id="header">
			<div id="header_wrap">
				<div id="header_top">
					<div id="header_logo" onclick="location.href='/'">PASTEBIN</div>
                    <div id="header_links">
                                                    <a href="/pro" class="mmh">GO <img src="/i/t.gif" alt="" class="header_pro_btn"></a>
                        
                        <a href="/api" class="mmh">API</a>
                        <a href="/tools" class="mmh">TOOLS</a>
                        <a href="/faq" class="mmh">FAQ</a>
                        <a href="https://deals.pastebin.com" target="_blank" class="mmh">DEALS</a>
                    </div>
                    <div id="header_search">
                        <form class="search_form" name="search_form" method="get" action="/search" id="cse-search-box">
                            <input class="search_input" type="text" name="q" size="5" value="" placeholder="Search..." />
                        </form>
                    </div>
                    <div id="header_new_paste" class="new_paste_button" onclick="location.href='/'">paste</div>
                    
			<div class="header_sign">
				<a href="<?php echo env('APP_URL');?>/login" class="btn-sign sign-in">SIGN IN</a>
				<a href="<?php echo env('APP_URL')?>/signup" class="btn-sign sign-up">SIGN UP</a>
			</div>				</div>
			</div>
		</div>
		<div id="super_frame">
			<div id="monster_frame">
				<div id="content_frame">
					<div id="content_right">						

		<div id="abrpm2"></div>
	
			<div style="padding: 0; width:160px;margin: 10px 0;clear:left;">
				<script type="text/javascript"><!--
					e9 = new Object();
					e9.size = "160x600,120x600";
				//--></script>
				<script type="text/javascript" src="//tags.expo9.exponential.com/tags/PastebincomNew/Sure/tags.js"></script>
			</div>
<div id="steadfast" title="Pastebin is proudly hosted by Steadfast.net" onclick="location.href='http://steadfast.net/?utm_source=pastebin.com&amp;utm_medium=referral&amp;utm_content=hosting_by_banner&amp;utm_campaign=referral_20140118_x_x_pastebin_partner&amp;source=referral_20140118_x_x_pastebin_partner'"></div>
	</div>
	<div id="content_left"><div id="ie_msg"></div>
					
		
			<div id="abrpm"></div>
			<div class="banner_728">
				<a href="https://billing.ivacy.com/page/91104" target="_blank"><img src="adserver/i/728x90_ivacy_2.png" width="728" height="90" border="0" alt="" /></a>
			</div>
			<div class="layout_clear"></div>
	<div class="content_title">This page has been removed!</div>
	<div class="content_text" style="padding:10px 0 0 0">
		<div id="notice" style="margin:0;">This page is no longer available. It has either expired, been removed by its creator, or removed by one of the Pastebin staff.</div>

	</div>
	<script type="text/javascript">
	$(document).ready(function(){
		$(".close1").click(function(){return $("#float-box-1").hide(),createCookie("l2c_1",!0,90),!1});
		$(".cookie_button").click(function(){return $("#float-box-1").hide(),createCookie("l2c_1",!0,90),!1});		
		$(".close2").click(function(){return $("#float-box-2").hide(),createCookie("l2c_2",!0,14),!1});
		$(".close3").click(function(){return $("#float-box-3").hide(),createCookie("l2c_2",!0,14),!1});
		$(".close4").click(function(){return $("#float-box-4").hide(),createCookie("l2c_4",!0,7),!1});
	});	
	</script>
	<div id="float-box-frame">		
		<div id="float-box-1">
			We use cookies for various purposes including analytics. By continuing to use Pastebin, you agree to our use of cookies as described in the <a href="/doc_cookies_policy">Cookies Policy</a>. <span class="cookie_button">OK, I Understand</span>
		</div>
		<div id="float-box-3">
			<style>
				#pro_promo_text{color:#333;float:right;display:block;font-size:14px;font-weight:400;width:270px}					
				#pro_promo_img {background: #fff;width: 60px;height: 50px;float: left;}
			</style>			
			<div id="pro_promo_img"><a href="/signup"><img src="/i/hello.png" alt="" border="0" style="width:48px;height:48px;" /></a></div> 
			<div id="pro_promo_text">Not a member of Pastebin yet?<br /><a href="/signup" style="text-decoration: underline dotted"><b>Sign Up</b></a>, it unlocks many cool features!</div>
			<div class="close3" title="Close Me">&nbsp;</div>
		</div></div>						</div>
					</div>
				</div>
			</div>
			<div id="mid_footer">
				<a href="/tools#chrome" title="Google Chrome Extension"><img src="/i/t.gif" alt="" class="icon24 chrome" /></a>
				<a href="/tools#firefox" title="Firefox Extension"><img src="/i/t.gif" alt="" class="icon24 firefox" /></a>
				<a href="/tools#iphone" title="iPhone/iPad Application"><img src="/i/t.gif" alt="" class="icon24 iphone" /></a>
				<a href="/tools#windows" title="Windows Desktop Application"><img src="/i/t.gif" alt="" class="icon24 windows" /></a>
				<a href="/tools#android" title="Android Application"><img src="/i/t.gif" alt="" class="icon24 android" /></a>
				<a href="/tools#macos" title="MacOS X Widget"><img src="/i/t.gif" alt="" class="icon24 macos" /></a>
				<a href="/tools#opera" title="Opera Extension"><img src="/i/t.gif" alt="" class="icon24 opera" /></a>
				<a href="/tools#pastebincl" title="Linux Application"><img src="/i/t.gif" alt="" class="icon24 unix" /></a>
			</div> 
		</div>
		<div id="footer">
			<div id="footer_links">
				<a href="/">create new paste</a> &nbsp;/&nbsp; 
				<a href="https://deals.pastebin.com">deals</a><sup style="color:red">new!</sup> &nbsp;/&nbsp; 
				<a href="/languages">syntax languages</a> &nbsp;/&nbsp; 
				<a href="/archive">archive</a> &nbsp;/&nbsp; 
				<a href="/faq">faq</a> &nbsp;/&nbsp; 
				<a href="/tools">tools</a> &nbsp;/&nbsp; 
				<a href="/night_mode">night mode</a> &nbsp;/&nbsp; 
				<a href="/api">api</a> &nbsp;/&nbsp; 
				<a href="/doc_scraping_api">scraping api</a>		

				<br />
				<a href="/doc_privacy_statement">privacy statement</a> &nbsp;/&nbsp; 
				<a href="/doc_cookies_policy">cookies policy</a> &nbsp;/&nbsp; 
				<a href="/doc_terms_of_service">terms of service</a> &nbsp;/&nbsp; 
				<a href="/doc_security_disclosure">security disclosure</a> &nbsp;/&nbsp; 
				<a href="/dmca">dmca</a> &nbsp;/&nbsp; 
				<a href="/contact">contact</a>
				<br /><br />
				<span class="h_800">
					By using Pastebin.com you agree to our <a href="/doc_cookies_policy">cookies policy</a> to enhance your experience.
					<br />
					Site design &amp; logo &copy; 2019 Pastebin; user contributions (pastes) licensed under <a href="http://creativecommons.org/licenses/by-sa/3.0/" target="_blank" rel="nofollow">cc by-sa 3.0</a> -- <a href="https://favpng.com" title="Free Transparent PNG Images" target="_blank">FavPNG</a> --
				</span>
				<a href="http://steadfast.net/services/dedicated-servers.php?utm_source=pastebin.com&amp;utm_medium=referral&amp;utm_content=footer_link_dedicated_server_hosting_by&amp;utm_campaign=referral_20140118_x_x_pastebin_partner&amp;source=referral_20140118_x_x_pastebin_partner" rel="nofollow" target="_blank">Dedicated Server Hosting</a> by <a href="http://steadfast.net/?utm_source=pastebin.com&amp;utm_medium=referral&amp;utm_content=footer_link_steadfast&amp;utm_campaign=referral_20140118_x_x_pastebin_partner&amp;source=referral_20140118_x_x_pastebin_partner" rel="nofollow" target="_blank">Steadfast</a>
			</div>
			<div id="footer_right" class="h_1024">
			
				<a href="https://facebook.com/pastebin" rel="nofollow" title="Like us on Facebook" target="_blank"><img src="/i/t.gif" alt="" class="icon40 facebook_circle" /></a>
				<a href="https://twitter.com/pastebin" rel="nofollow" title="Follow us on Twitter" target="_blank"><img src="/i/t.gif" alt="" class="icon40 twitter_circle" /></a>
							</div>
		</div>

			<script type="text/javascript"><!--
			  e9 = new Object();
			  e9.snackbar = true;
			//--></script>
			<script type="text/javascript" src="//tags.expo9.exponential.com/tags/PastebincomNew/SnackbarSafe/tags.js"></script>
			<script src="//m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>
			<script>
			(function() {
			  if (typeof _bsa !== 'undefined' && _bsa) {
				_bsa.init('fancybar', 'CKYDL2JL', 'placement:pastebincom');
			  }
			})();
			</script>
		<script type="text/javascript">
			function abdd() {$("#adb-enabled").show(), $("#adb-not-enabled").hide(), $("#abrpm3").html("<center><iframe src='/pba/cube_safe.php' id='monkey_3' marginwidth='0' align='center' marginheight='0' hspace='0' vspace='0' frameborder='0' scrolling='no' allowtransparency='true' width='300' height='250' style='width: 300px; height: 250px; margin-top:10px;'></iframe></center>"), $("#abrpm2").html("<iframe src='/pba/sky_safe.php' id='monkey' marginwidth='0' align='center' marginheight='0' hspace='0' vspace='0' frameborder='0' scrolling='no' allowtransparency='true' width='160' height='600' style='width: 160px; height: 600px;'></iframe>"), $("#abrpm").html("<iframe src='/pba/lead_safe.php' id='monkey_2' marginwidth='0' align='center' marginheight='0' hspace='0' vspace='0' frameborder='0' scrolling='no' allowtransparency='true' width='728' height='100' style='width: 728px; height: 100px;'></iframe>")}
			function abnd(){$("#adb-enabled").hide(), $("#adb-not-enabled").show()}$(function(){}), "undefined" == typeof fuckAdBlock ? abdd() : (fuckAdBlock.setOption({debug: !1}), fuckAdBlock.onDetected(abdd).onNotDetected(abnd));
			var $title=$("a,input,p,label,textarea[title],img,button,span");$.each($title,function(){$(this).tooltip({show:{delay:1},hide:{delay:1}})});
		</script>		<script type="text/javascript">
			function isIE(){var e=navigator.userAgent.toLowerCase();return-1!=e.indexOf("msie")?parseInt(e.split("msie")[1]):!1}if(isIE()&&isIE()<10){var div=document.getElementById("ie_msg");div.innerHTML=div.innerHTML+'<div id="old_browser">Your browser is outdated and insecure! Pastebin does not offer support for your browser. <a href="http://outdatedbrowser.com/" target="_blank" rel="nofollow">Click here to update your browser</a>!</div>'}
		</script>
		<div class="pub_300x250"></div><a href="#0" class="cd-top">Top</a>

		<!-- /7346874/Hellobar-adunits/176 -->
<div id='div-gpt-ad-1565040785112-0' style='width: 1px; height: 1px;'>
  <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1565040785112-0'); });
  </script>
</div>


	</body>
</html>