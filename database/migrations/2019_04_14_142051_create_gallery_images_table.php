<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_images', function (Blueprint $table) {
            $table->unsignedBigInteger('id_gallery');
            $table->unsignedBigInteger('id_image');

            $table->primary(['id_gallery', 'id_image']);
            $table->timestamps();

            $table->foreign('id_gallery')
                ->references('id')->on('galleries')
                ->onDelete('cascade');

            $table->foreign('id_image')
                ->references('id')->on('images')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallery_images');
    }
}
