<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Ranking
 * @package App\Models
 * @version April 15, 2019, 7:25 pm UTC
 *
 * @property \App\Models\Edition idEdition
 * @property \App\Models\User idPlayer
 * @property \Illuminate\Database\Eloquent\Collection 
 * @property integer id_edition
 * @property integer id_player
 */
class Ranking extends Model
{
    //use SoftDeletes;

    public $table = 'rankings';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $primaryKey = "id";

    protected $dates = ['deleted_at'];


    public $fillable = [
        'year',
        'date',
        'id_player',
        'points',
        'id_city',
        'id_match',
        'id_edition',
        'match_won',
        'match_lost',
        'match_deuce',
        'set_won',
        'set_lost',
        'games_won',
        'games_lost'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'year' => 'integer',
        'date' => 'date',
        'id_player' => 'integer',
        'points' => 'integer',
        'id_city' => 'integer',
        'id_match' => 'integer',
        'id_edition' => 'integer',
        'match_won' => 'integer',
        'match_lost' => 'integer',
        'match_deuce' => 'integer',
        'set_won' => 'integer',
        'set_lost' => 'integer',
        'games_won' => 'integer',
        'games_lost' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'year' => 'required',
        'date' => 'required',
        'id_player' => 'required',
        'points' => 'required',
        'id_city' => 'required',
        'id_match' => 'required',
        'id_edition' => 'required',
        'match_won' => 'required',
        'match_lost' => 'required',
        'match_deuce' => 'required',
        'set_won' => 'required',
        'set_lost' => 'required',
        'games_won' => 'required',
        'games_lost' => 'required'
    ];
    

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function player()
    {
        return $this->belongsTo(\App\Models\User::class, 'id_player');
    }
}
