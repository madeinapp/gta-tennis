<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;

use App\Http\Requests\CreateTeamRequest;
use App\Http\Requests\UpdateTeamRequest;
use App\Repositories\TeamRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\Team;
use App\Models\TeamPlayer;

class TeamController extends AppBaseController
{
    /** @var  TeamRepository */
    private $teamRepository;

    public function __construct(TeamRepository $teamRepo)
    {
        $this->teamRepository = $teamRepo;
    }

    /**
     * Display a listing of the Team.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $teams = $this->teamRepository->all();    
        
        return view('admin.teams.index')
            ->with('teams', $teams);
    }

    /**
     * Show the form for creating a new Team.
     *
     * @return Response
     */
    public function create()
    {        
        $team = new Team();
        $team->save();
        $team->name = 'Squadra' . $team->id;
        $team->save();

        $teamPlayer = new TeamPlayer();
        $teamPlayer->id_team = $team->id;
        $teamPlayer->id_player = Auth::id();
        $teamPlayer->starter = true;
        $teamPlayer->save();
        
        return redirect(route('admin.teams.edit', ['id' => $team->id]));        
    }

    /**
     * Store a newly created Team in storage.
     *
     * @param CreateTeamRequest $request
     *
     * @return Response
     */
    public function store(CreateTeamRequest $request)
    {
        $input = $request->all();

        $team = $this->teamRepository->create($input);

        Flash::success('Team saved successfully.');

        //return redirect(route('admin.teams.index'));
        return redirect(route('admin.subscription', ['id_tournament' => $input['id_tournament']]));
    }

    /**
     * Display the specified Team.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $team = $this->teamRepository->find($id);

        if (empty($team)) {
            Flash::error('Team not found');

            return redirect(route('admin.teams.index'));
        }

        return view('admin.teams.show')->with('team', $team);
    }

    /**
     * Show the form for editing the specified Team.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {        
        
        $team = Team::where('id', '=', $id)->first();        
        $players = TeamPlayer::with('player')
                                ->where('id_team', '=', $id)                
                                ->get();

        return view('admin.tournaments.subscription')
                ->with('team', $team)
                ->with('players', $players);

    }

    /**
     * Update the specified Team in storage.
     *
     * @param int $id
     * @param UpdateTeamRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTeamRequest $request)
    {
        $input = $request->all();
        
        $team = $this->teamRepository->find($id);

        if (empty($team)) {
            Flash::error('Team not found');

            return redirect(route('admin.teams.index'));
        }

        $team = $this->teamRepository->update($request->all(), $id);

        Flash::success('Team updated successfully.');

        return redirect(route('admin.subscription', ['id_tournament' => $input['id_tournament']]));
    }

    /**
     * Remove the specified Team from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        $team = $this->teamRepository->find($id);
        //dd($request->getRequestUri());
        if (empty($team)) {
            Flash::error('Team not found');

            return redirect(route('admin.teams.index'));
        }

        $this->teamRepository->delete($id);

        Flash::success('Team deleted successfully.');

        return redirect(route('admin.teams.index'));
    }


    public function destroyMyTeam(Request $request, $id)
    {
        $team = $this->teamRepository->find($id);
        
        if (empty($team)) {
            Flash::error('Team not found');

            return redirect(route('admin.teams.myteams'));
        }

        $this->teamRepository->delete($id);

        Flash::success('Team deleted successfully.');

        return redirect(route('admin.teams.myteams'));
    }


    public function myTeams(){
        $teams = TeamPlayer::where('id_player', '=', Auth::id())->with('team')->get();
        return view('admin.teams.myteams')->with('teams', $teams);
    }


    public function changePlayer(Request $request){
        $input = $request->all();
        $post_players = [];
        foreach($input as $k => $val):
            if( substr($k, 0, 6) == 'player'):
                $post_players[] = substr(substr($k, 7), 0, strlen(substr($k, 7))-8 );
            endif;
        endforeach;
        
        if(count($post_players) != 2 ):
            return back()->withInput()->withErrors(['I titolari devono essere 2']);            
        else:
            $team = Team::where('id', '=', $input['id_team'])->first();
            $old_starters = [];
            foreach($team->players as $player):                
                if($player->starter):                    
                    $old_starters[] = $player->player->id;                    
                endif;
            endforeach;

            sort($old_starters);
            sort($post_players);            

            if($old_starters == $post_players):
                return back()->withInput()->withErrors(['Non hai effettuato nessuna sostituzione']);            
            endif;

            foreach($team->players as $player):                
                if($player->starter):                    
                    $player->starter = false;
                    $player->save();    
                endif;
            endforeach;            

            foreach($team->players as $player):                                
                if(in_array( (string)$player->player->id, $post_players)):
                    $player->starter = true;
                    $player->save();                    
                endif;
            endforeach;

            $team->flag_change = true; 
            $team->save();
            
            return back();
        endif;
    }
}
