<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;

use App\Http\Requests\CreateRankingRequest;
use App\Http\Requests\UpdateRankingRequest;
use App\Repositories\RankingRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\Ranking;
use App\Models\User;
use App\Models\Match;
use App\Models\Score;
use App\Models\Team;
use App\Models\TeamPlayer;

use Carbon\Carbon;

class RankingController extends AppBaseController
{
    /** @var  RankingRepository */
    private $rankingRepository;

    public function __construct(RankingRepository $rankingRepo)
    {
        $this->rankingRepository = $rankingRepo;
    }

    /**
     * Display a listing of the Ranking.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index($year)    
    {
        $rankings = Ranking::selectRaw('sum(points) as points, year, id_player')
                            ->where('year', '=', $year)
                            ->groupBy('year', 'id_player')
                            ->orderBy('points', 'DESC')
                            ->get();

        $years = DB::table('rankings')
                        ->select('year')
                        ->groupBy('year')
                        ->take('5')                        
                        ->get()
                        ->pluck('year', 'year')
                        ->toArray();        
        
        return view('admin.rankings.index')
            ->with('rankings', $rankings)
            ->with('years', $years)
            ->with('sel_year', $year)
            ;
    }    

    public function live()    
    {
        $rankings = Ranking::selectRaw('sum(points) as points, id_player')
                            ->where('date', '>', Carbon::now()->subYear(1))
                            ->groupBy('id_player')
                            ->orderBy('points', 'DESC')
                            ->get();        
        
        return view('admin.rankings.live')
            ->with('rankings', $rankings)                        
            ;
    }    
}
