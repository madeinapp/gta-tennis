
<div class="row">
    <div class="col-xs-6">    
        <div class="input-group">
            <form class="typeahead" role="search">
                <div class="form-group">
                    <input type="search" name="q" id="search-input" class="form-control search-input" placeholder="Aggiungi giocatore" autocomplete="off">
                </div>
            </form>
            <div id="team-player-add" class="input-group-btn">

                <input type="hidden" id="id_tournament" name="id_tournament" value="{!! $tournament->id !!}">
                <input type="hidden" id="id_team" name="id_team" value="{!! $team->id !!}">
                <input type="hidden" id="id_player" name="id_player">
                <input type="hidden" id="starter" name="starter" value="0">
                <button type="button" class="btn btn-primary" style="margin: 0"" onClick="addTeamPlayer()"><i class="fa fa-plus"></i> Aggiungi</button>
            
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <!-- Name Field -->
        <table class="table table-striped">
            <thead>
                <td></td>
                <td>Giocatore</td>
                <td>Email</td>
                <td>Titolare</td>
            </thead>
            <tbody id="tbody-team-players">
                @foreach( $players as $player)
                <tr id="player_{!! $player->id_player !!}">
                    <td style="width: 55px;"><img src="https://via.placeholder.com/50?text=?" class="img-circle"></td>
                    <td class="text-left">{!! $player->player->name !!} {!! $player->player->surname !!}</td>
                    <td>{!! $player->player->email !!}</td>
                    <td>
                    @if($player->starter) 
                    <label class="label label-success">Titolare</label>
                    @else 
                    <label class="label label-danger">Riserva</label>
                    @endif
                    </td>
                    <td>
                        @if( $player->player->id != Auth::id() )                                                
                        <button class="btn btn-danger" onClick="removeTeamPlayer({!! $player->id !!})"><i class="fa fa-trash"></i></button>
                        @endif
                    </td>
                </tr>        
                @endforeach
            </tbody>
        </table>
    </div>
</div>

