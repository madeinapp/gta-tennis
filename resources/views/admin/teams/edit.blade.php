@extends('admin.layouts.app')

@section('css')
<style>
.twitter-typeahead, .tt-hint, .tt-input, .tt-menu { width: 100%; margin-top: 10px; }
</style>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            {{ $team->name }}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">

                {!! Form::model($team, ['route' => ['admin.teams.update', $team->id], 'method' => 'patch']) !!}
                    
                    @include('admin.teams.fields')
                
                {!! Form::close() !!}
            
           </div>
       </div>
   </div>
@endsection

@section('scripts')
<script>

showModalPlayer = function(id_team){
    $("#modal-player #id_team").val(id_team);
    $("#modal-player").modal('show');
}

selectPlayer = function(id_player){
    $("#id_player").val(id_player);   
}

$(document).ready(function($) {
    // Set the Options for "Bloodhound" suggestion engine
    var engine = new Bloodhound({
        remote: {
            url: '{{ env('APP_URL') }}/admin/player/search?q=%QUERY%',
            wildcard: '%QUERY%'
        },
        datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });

    $(".search-input").change(function(){
        $("#player_id").val('');
    });

    $(".search-input").typeahead({
        hint: true,
        highlight: true,
        minLength: 3,
        afterSelect: function(item) {
            console.log("afterSelect", item, this.$element);
            this.$element[0].value = item.value
        }
    }, {
        source: engine.ttAdapter(),

        // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
        name: 'usersList',

        display: function(data){ return data.name },

        // the key from the array we want to display (name,id,email,etc...)
        templates: {
            
            empty: [
                '<div class="list-group search-results-dropdown"><div class="list-group-item">Nothing found.</div></div>'
            ],
            header: [
                '<div class="list-group search-results-dropdown">'
            ],
            suggestion: function (data) {
                return '<a href="#" onClick="selectPlayer('+data.id+')" class="list-group-item">' + data.name + ' <br> ' + data.email + '</a>'
            }
        }
    });
});
</script>
@endsection