@extends('admin.layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Assegna punti rankings</h1>        
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">      
                <form id="frm_sel_edition" action="" method="GET">
                    <div class="form-group">                        
                        {!! Form::label('id_edition', 'Torneo') !!}
                        {!! Form::select('id_edition', $editions, $selected_edition, ['class' => 'form-control' , 'required' => true ]) !!}
                    </div>
                </form>

                @if( !empty($subscriptions))
                <form action="{{ route('admin.rankings.store') }}" method="POST">

                    @csrf

                    @php 
                    $last_zone = null;
                    $last_category_type = null;
                    $last_category = null;
                    @endphp
                    <table class="table table-striped"> 
                        <thead>
                        <tr>
                            <th>Zona</th>
                            <th>Tipologia</th>
                            <th>Categoria</th>
                            <th>Giocatore</th>
                            <th>Assegma punti</th>
                        </tr>                       
                        </thead>
                    @foreach ($subscriptions as $subscription)
                        @foreach($subscription->team->players as $teamPlayer)
                        <tr>                            
                            <td>{{ $subscription->zone->city->country->name }} - {{ $subscription->zone->city->name }}</td>                            
                            <td>{{ $subscription->categoryType->name }}</td>
                            <td>{{ $subscription->category->name }}</td>
                            <td>{{ $teamPlayer->player->name }} {{ $teamPlayer->player->surname }}</td>                            
                            <td>
                                @php
                                $points = null; 
                                if(in_array($edition->edition_type, [0,1])):
                                    $points = \App\Models\Ranking::where('id_edition', $selected_edition)
                                                                    ->whereNull('id_match')
                                                                    ->where('id_player', $teamPlayer->id_player)
                                                                    ->first();                                    
                                endif;
                                @endphp
                                {!! Form::number($teamPlayer->player->id.'-'.$subscription->zone->city->id, (!empty($points) ? $points->points : null), ['class' => 'form-control', 'placeholder' => 'Assegna punti']) !!}
                            </td>
                        </tr>    
                        @endforeach
                    @endforeach
                    </table>

                    <div class="form-group">
                        {!! Form::hidden('id_edition', $selected_edition) !!}
                        <button type="submit" class="btn btn-lg btn-success">Salva punti</button>
                    </div>
                </form>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){       
        $("#id_edition").on("change", function(){
            $("#frm_sel_edition").submit();
        });
    });
</script>
@endsection
